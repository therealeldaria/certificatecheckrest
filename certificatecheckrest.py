import json
import requests
import string
import secrets
import configuration
import logging
from datetime import datetime
import smtplib
from string import Template
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

myconfig = configuration.Config("./config/config.yaml")
myconfig.load()

clientCrt = myconfig.get('certificate_path.client_certificate')
clientKey = myconfig.get('certificate_path.certificate_private_key')
CA = myconfig.get('certificate_path.ca_certificate')
numDays = myconfig.get('parameters.days_to_check')
serverAddress = myconfig.get('parameters.ejbca_server_address')
batch_size = myconfig.get('parameters.rest_batch_size')
enrollment_key_length = myconfig.get('parameters.enrollment_key_length')

loggLevel = "info"
debugMode = True  # Set to True to not send or update any records.
testsson = False  # Renew certificate for Test Testsson

# Setup Logging
num_loglevel = getattr(logging, loggLevel.upper(), None)
if not isinstance(num_loglevel, int):
    raise ValueError('Invalid log level: %s' % loggLevel)
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    filename='certificatecheckrest.log',
    encoding='utf-8',
    level=num_loglevel
)


# Function for creating secure password
def rand_pass(size, scope=string.ascii_letters + string.digits):
    password = ''.join(secrets.choice(scope) for c in range(size))
    logging.debug('Generated random password: %s', password)
    return password


# Get amount of certs that expire
def gettotalcerts():
    api_url = "https://" + serverAddress + "/ejbca/ejbca-rest-api/v1/certificate/expire?days=" + str(numDays)
    response = requests.get(api_url, verify=CA, cert=(clientCrt, clientKey))
    return json.loads(response.text)["pagination_rest_response_component"]["number_of_results"]


# Get a list of certs that are about to expire.
def getexpiredcerts(offset_id=0, amount=1):
    api_url = "https://" + serverAddress + "/ejbca/ejbca-rest-api/v1/certificate/expire?days=" + str(
        numDays) + "&maxNumberOfResults=" + str(amount) + "&offset=" + str(offset_id)
    response = requests.get(api_url, verify=CA, cert=(clientCrt, clientKey))
    return json.loads(response.text)


# Extract serial numbers of retrived certificates.
def getbatchserials(cert_batch):
    certserials = []
    for cert in cert_batch["certificates_rest_response"]["certificates"]:
        certserials.append(cert["serial_number"])
    return certserials


# Get details from api, certificate or endendity.
def getdetails(details_id, details_type, max_results):
    api_url = ""
    payload = {"max_number_of_results": max_results, "pagination": {"page_size": 1000, "current_page": 1},
               "criteria": [{"property": "QUERY", "value": details_id, "operation": "EQUAL"}]}
    headers = {'content-type': 'application/json'}

    if details_type == "certificatev2":
        logging.debug('Looking up certificate %s', details_id)
        api_url = "https://" + serverAddress + "/ejbca/ejbca-rest-api/v2/certificate/search"
    elif details_type == "endentity":
        logging.debug('Looking up end entity %s', details_id)
        api_url = "https://" + serverAddress + "/ejbca/ejbca-rest-api/v1/endentity/search"

    response = requests.post(api_url, data=json.dumps(payload), verify=CA, headers=headers, cert=(clientCrt, clientKey))
    return response


# Function to do the actual processing.
def process_batch(batch):
    for serial in getbatchserials(batch):
        parsed_certificate = json.loads(getdetails(serial, "certificatev2", 1).text)
        if len(parsed_certificate["certificates"]) > 0:
            parsed_endentity = json.loads(getdetails(parsed_certificate["certificates"][0]["subjectDN"], "endentity", 1)
                                          .text)
            if len(parsed_endentity["end_entities"]) > 0:
                user = parsed_endentity["end_entities"][0]["username"]
                expiredate = datetime.fromtimestamp(parsed_certificate["certificates"][0]["expireDate"] /
                                                    1000).isoformat()
                logging.info('Certificate with serial %s belonging to %s is expiring on %s.', serial, user, expiredate)
                if parsed_endentity["end_entities"][0]["status"] == "NEW":
                    logging.info('Certificate already issued but has not yet been retrieved.')
                elif checkforvalidcerts(parsed_endentity["end_entities"][0]["dn"]):
                    logging.info('User have another valid certificate that does not expire within %s days',
                                 str(numDays))
                else:
                    logging.info('Issuing new certificate to %s', user)
                    if parsed_endentity["end_entities"][0]["email"] is not None:
                        user_data = {
                            'Username': parsed_endentity["end_entities"][0]["username"],
                            'Common Name': parsed_endentity["end_entities"][0]["username"],
                            'E-mail': parsed_endentity["end_entities"][0]["email"]
                        }
                        status = seteestatusnew(user_data['Username'], enrollment_key_length)
                        if status['Status'] == 200:
                            send_email(
                                'Get your new PrimeKey certificate.',
                                myconfig.get('templates.renewal_template'),
                                user_data, status['Password']
                            )
                    else:
                        logging.warning('User %s does not have an e-mail associated,'
                                     ' won\'t renew cert, please correct then re-run script',
                                     parsed_endentity["end_entities"][0]["username"])
            else:
                subjectdn = parsed_certificate["certificates"][0]["subjectDN"]
                expiredate = datetime.fromtimestamp(parsed_certificate["certificates"][0]["expireDate"]
                                                    / 1000).isoformat()
                logging.warning('Certificate with Serial %s and SubjectDN %s expires on %s \n'
                              'Unable to find an End Entity associated with this certificate, Please verify SubjectDN',
                              serial, subjectdn, expiredate)


# Verify if user has any other valid certificates.
def checkforvalidcerts(dn):
    certs = json.loads(getdetails(dn, "certificatev2", 1).text)
    for cert in certs["certificates"]:
        if cert["expireDate"] / 1000 > datetime.utcnow().timestamp() + numDays * 86400:
            return True
    return False


# Update End Entity Status
def seteestatusnew(endendity, key_length):
    password = rand_pass(key_length)
    api_url = "https://" + serverAddress + "/ejbca/ejbca-rest-api/v1/endentity/" + endendity + "/setstatus"
    payload = {"password": password, "token": "P12", "status": "NEW"}
    headers = {'content-type': 'application/json'}
    if not debugMode:
        response = requests.post(
            api_url,
            data=json.dumps(payload),
            verify=CA,
            headers=headers,
            cert=(clientCrt, clientKey)
        )
        return {'Status': response.status_code, 'Password': password}
    else:
        logging.info('Would have set status NEW for user %s, but debug is enabled', endendity)
        return {'Status': 200, 'Password': 'DebugMode'}


# Returns a Template object containing the contents of the file specified
def read_template(filename):
    with open('email_templates/' + filename, 'r', encoding='utf-8') as template_file:
        template_file_content = template_file.read()
    return Template(template_file_content)


def send_email(mailsubject, message_template, user_data, enrollment_key):

    # Set up smtp server connection
    hostname = myconfig.get('smtp.smtp_hostname')
    port = myconfig.get('smtp.smtp_port')
    username = myconfig.get('smtp.smtp_username')
    password = myconfig.get('smtp.smtp_password')
    mailfrom = myconfig.get('smtp.smtp_mailfrom')

    s = smtplib.SMTP(host=hostname, port=port)
    s.starttls()
    s.login(user=username, password=password)

    # create a message
    mime_msg = MIMEMultipart()
    templatedict = {"PERSON_NAME": user_data['Common Name'].split()[0].title(), "USER_NAME": user_data['Username'],
                    "ENROLMENT_KEY": enrollment_key}

    # read and add in template values
    message_template = read_template(message_template)
    message = message_template.substitute(**templatedict)

    # set up the parameters of the message
    mime_msg['From'] = mailfrom
    mime_msg['To'] = user_data['E-mail']
    mime_msg['Subject'] = mailsubject

    # add in the message body
    mime_msg.attach(MIMEText(message, 'html'))

    # send the message via the server set up earlier.
    if not debugMode:
        s.send_message(mime_msg)
    else:
        logging.info('Would have sent message to %s using e-mail %s, but debug is enabled',
                     user_data['Username'], user_data['E-mail'])


# Main process.
def main():
    logging.info('Checking for certificates that expires within %s days.', str(numDays))
    logging.info('Getting first batch')
    logging.debug('Batch size set to %s items.', str(batch_size))
    current_batch = getexpiredcerts(0, batch_size)
    while True:
        process_batch(current_batch)
        if current_batch["pagination_rest_response_component"]["more_results"]:
            current_batch = getexpiredcerts(current_batch["pagination_rest_response_component"]["next_offset"],
                                            batch_size)
            logging.info('There are additional items, Getting next batch.')
        else:
            break


main()
