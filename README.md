---- No longer working on this. ----


A Python script to automatically renew certificates in EJBCA
- It will grap any certificates that are about to expire in the configured time period.
- Will try to retrieve details like Username, CN, UID, and E-mail from the old certificate/End Entity.
- Request renewal of certificate by setting End Entity to New, and generating a random password.
- Send mail according to template to the user that there is a new certificate to pick up.

~~TODO:
- Make so Log of last run is sent to an admin mail to enable unattended runs with for example chron.
- Command line usage to renew a specific user.
- Ability to issue new certificates.~~
